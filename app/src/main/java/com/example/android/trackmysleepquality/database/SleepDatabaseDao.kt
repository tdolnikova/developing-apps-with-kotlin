/*
 * Copyright 2018, The Android Open Source Project
 */

package com.example.android.trackmysleepquality.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface SleepDatabaseDao {
    @Insert
    fun insert(sleepNight: SleepNight)

    @Update
    fun update(sleepNight: SleepNight)

    @Query("select * from sleep_night where id = :id")
    fun get(id: Long): SleepNight

    @Query("delete from sleep_night")
    fun clear()

    @Query("select * from sleep_night order by id desc")
    fun getAllNights(): LiveData<List<SleepNight>>

    @Query("select * from sleep_night order by id limit 1")
    fun getTonight(): SleepNight?

    @Query("select * from daily_sleep_quality_table where nightId = :key")
    fun getNightWithId(key: Long): LiveData<SleepNight>


}
